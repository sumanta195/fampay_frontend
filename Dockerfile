# Install dependencies only when needed
FROM node:14-alpine

#Sets the working directory for any RUN, CMD, ENTRYPOINT, COPY, and ADD commands
WORKDIR /app

# RUN rm -r /app/.next/*

#Copy new files or directories into the filesystem of the container
COPY package.json /app
COPY yarn.lock /app

#Execute commands in a new layer on top of the current image and commit the results
RUN npm install

##Copy new files or directories into the filesystem of the container
COPY . /app


#Informs container runtime that the container listens on the specified network ports at runtime
EXPOSE 3000

CMD ["npm", "start"]