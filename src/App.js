import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Dashboard from './dashboard/Dashboard'
import home from './home/home';
import search from './search/search'
import Videos from './videos/Videos'

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <BrowserRouter>
        <Switch>
            <Route path="/" exact component={home}/>
            <Route path="/dashboard" exact component={Dashboard}/>
            <Route path="/search" exact component={search}/>
            <Route path="/videos" exact component={Videos}/>
        </Switch>
      </BrowserRouter>


        
      </header>
    </div>
  );
}

export default App;
