import React, {useState} from 'react';
import Datatable from 'react-bs-datatable';

// Create table headers consisting of 4 columns.
const header = [
  { title: 'Video ID', prop: 'videoId', filterable: true, sortable: true},
  { title: 'Title', prop: 'videoTitle',filterable: true, sortable: true },
  { title: 'Description', prop: 'videoDescription', filterable: true, sortable: true },
  { title: 'Published At', prop: 'publishTime', filterable: true, sortable: true },
  { title: 'Thumbnail', prop: 'thumbnail', sortable: true }
];

function VideoTable(props) {
    return (
        <>
            <Datatable tableHeaders={header} tableBody={props.data} rowsPerPage={5} />
        </>
        
    )
}

export default VideoTable