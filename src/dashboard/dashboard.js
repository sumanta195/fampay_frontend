import React, { useState, useEffect } from 'react'
import VideoTable from './VideoTable'

function Dashboard(){
    const [dataHolder, setDataHolder] = useState([])
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          fetch("http://127.0.0.1:5000/api/all", requestOptions)
            .then(response => response.json())
            .then(result => {
                setDataHolder(result)
                setLoading(true)
            })
            .catch(error => console.log('error', error));
    },[]);
    return (
        <div>
            {
                (loading) ? (
                    <div className="row">
                        <div className="col-1"></div>
                        <div className="col-10 table-responsive"><VideoTable dataLength={dataHolder.data.length} data={dataHolder.data}/></div>
                        <div className="col-1"></div>
                    </div>
                ) : (
                    <div className="d-flex justify-content-center">
                        <div className="spinner-border" role="status" style={{'width': '10rem','height': '10rem'}}>
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                )
            }
            
        </div>
    )
}

export default Dashboard