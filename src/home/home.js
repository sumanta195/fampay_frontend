import React from 'react'

const home = function(){
    return (
        <div className="container">
            <div className="bg-light p-5 rounded">
                <h1>FamPay Assignment</h1>
                <p className="lead">A simple frontend to demonstrate API</p>
                <a className="btn btn-lg btn-primary" href="/" role="button">Home »</a>
            </div>
        </div>
    )
}

export default home