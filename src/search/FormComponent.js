import { isError } from 'lodash';
import React,{useState} from 'react' 
import VideoCards from '../videos/videoComponents/cards'



function FormComponent(){
    const [search, setSearch] = useState({keyWord:""});
    const [error, setError] = useState({isError: false, class: "", message: ""})
    const [btn, setBtn] = useState(true);
    const [loading, setLoading] = useState(false);
    const [resp, setResp] = useState([])
    const [gotData, setGotData] = useState(false)

    const handleSubmit=(e)=>{
        e.preventDefault()
        setBtn(false)
        console.log(search.keyWord.length)
        if(search.keyWord.length <= 2){
            setError({isError: true, class:"danger",message:"Please enter minimum 3 character for search."})
            setBtn(true)
        }else{
            setLoading(true)
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            let raw = JSON.stringify({ "keyword": `${search.keyWord}`});
            let requestOptions = { method: 'POST',headers: myHeaders,body: raw,redirect: 'follow'};
            fetch("http://127.0.0.1:5000/api/search", requestOptions)
            .then(response => response.json())
            .then(result => {
                setResp(result)
                setBtn(true)
                setLoading(false)
                setGotData(true)
            })
            .catch(error => console.log('error', error));
        }
    }
    return (
        <div className="container">
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>Search</label>
                    <input type="text" className="form-control" id="keyword" onChange={e=>setSearch({...search, keyWord:e.target.value})} value={search.keyWord}/>
                </div><br />
                <div className="form-group">
                {
                    (btn) ? (<button type="submit" className="btn btn-lg btn-primary">Search</button>) : 
                            (
                                <button type="submit" className="btn btn-lg btn-primary" disabled>
                                    <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp; Searching...
                                </button>
                            )
                }
                </div>
            </form>
            <div className="row">
            {
                    (error.isError)?(
                        (error.class === 'danger') ? (<div className="alert alert-danger">{error.message}</div>) : 
                            (error.class === 'info') ? (<div className="alert alert-info">{error.message}</div>) :
                                (error.class === 'success') ? (<div className="alert alert-success">{error.message}</div>) : (
                                    <span></span>
                                )
                    ):(
                        (loading) ? (
                            <div className="d-flex justify-content-center">
                                <div className="spinner-border" role="status" style={{'width': '10rem','height': '10rem'}}>
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        ) : (
                            (gotData) ? (
                                (resp.length) ? (
                                    resp.map(items => {
                                        return <VideoCards 
                                            key={items.videoId} 
                                            title={`${items.videoTitle.substring(0, 45)}`} 
                                            videoLink={`${items.videoId}`} 
                                            description={`${items.videoDescription.substring(0, 167)}`} 
                                            thumbnail={`${items.thumbnail}`}
                                        />
                                    })
                                ) : (<div className="alert alert-info">No Video(s) found with this keyword. <b>Searched Keyword: {`${search.keyWord}`}</b></div>)
                                
                                
                            ) : (<span></span>)
                        )
                        
                    )
                }
            </div>
        </div>
    )
}

export default FormComponent