import React from 'react'

const VideoCards = function(props){
    return (
        <>
            {console.log(props)}
                <div className="col">
                    <div className="card" style={{'width':'18rem'}}>
                        <img src={props.thumbnail} className="card-img-top" alt="thumbNail" />
                        <div className="card-body">
                            <h5 className="card-title">{props.title}</h5>
                            <p className="card-text">{props.description}</p>
                            <a href={`https://www.youtube.com/watch?v=${props.videoLink}`} className="btn btn-primary" target="_blank" rel="noreferrer">Watch on YouTube</a>
                        </div>
                    </div>
                </div>
        </>
    )
}

export default VideoCards