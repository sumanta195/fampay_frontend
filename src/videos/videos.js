
import React, { useState, useEffect } from 'react'
import VideoCards from './videoComponents/cards'




export default function Videos(){
    const [myvideos, setVideos] = useState([]);
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          fetch("http://127.0.0.1:5000/api/all", requestOptions)
            .then(response => response.json())
            .then(result => {
                setVideos(result)
                setLoading(true)
            })
            .catch(error => console.log('error', error));
    },[]);
    return (
        <div className="container">
            <div className="row">

{console.log("got data from API", myvideos)}
{
    (loading) ? (
        
        myvideos.data.map(items => {
            return <VideoCards 
                        key={items.videoId} 
                        title={`${items.videoTitle.substring(0, 45)}`} 
                        videoLink={`${items.videoId}`} 
                        description={`${items.videoDescription.substring(0, 167)}`} 
                        thumbnail={`${items.thumbnail}`}
                    />

        })
    ) : (
        <div className="d-flex justify-content-center">
            <div className="spinner-border" role="status" style={{'width': '10rem','height': '10rem'}}>
                <span className="visually-hidden">Loading...</span>
            </div>
        </div>
    )
}
</div>

        </div>
        )
}
